#include "stdafx.h"

void LINE::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, color);
	SelectObject(hdc, hPen);
	HBRUSH hBrush = CreateSolidBrush(_brush_color);
	SelectObject(hdc, hBrush);
	MoveToEx(hdc, left, top, NULL);
	LineTo(hdc, right, bottom);
	DeleteObject(hPen);
	DeleteObject(hBrush);
}

void LINE::WriteFile(ofstream &fo)
{
	type = 1;
	fo.write((char*)&type, sizeof(int));
	fo.write((char*)&left, sizeof(int));
	fo.write((char*)&top, sizeof(int));
	fo.write((char*)&right, sizeof(int));
	fo.write((char*)&bottom, sizeof(int));
	fo.write((char*)&color, sizeof(COLORREF));
}

void LINE::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }	
	switch (Type)	{
	case 0:
	{
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	}break;
	case 1:
	{		
		left = LOWORD(lParam);
		top = HIWORD(lParam);
	}break;
	case 2:
	{
		right = LOWORD(lParam);
		bottom = HIWORD(lParam);
	}	break;
	}
	InvalidateRect(hWnd, NULL, true);
}

bool LINE::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;

	a = right - left;
	b = bottom - top;
	func = b * (clickpoint.x - left) - a * (clickpoint.y - top);

	if (func >= 0 && func <= 2000 || func <= 0 && func >= -2000)
		return true;
	return false;
}

void LINE::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen = CreatePen(PS_SOLID, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Ellipse(hdc, left -RADIUS, top -RADIUS, left +RADIUS, top +RADIUS);
	Ellipse(hdc, right -RADIUS, bottom -RADIUS, right +RADIUS, bottom +RADIUS);
}

int LINE::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;
	rec.left = rec.right =left;
	rec.top = rec.bottom =top;
	if (isValidResize(lParam, rec))
		return 1;
	rec.left = rec.right =right;
	rec.top = rec.bottom =bottom;
	if (isValidResize(lParam, rec))
		return 2;
	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}

int LINE::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func, r;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r = 10;
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}


void RECTANGLE::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, this->color);
	SelectObject(hdc, hPen); 
	HBRUSH hBrush = CreateSolidBrush(_brush_color);
	MoveToEx(hdc, left, top, NULL);
	Rectangle(hdc, left, top, right,bottom);
	DeleteObject(hPen);
	DeleteObject(hBrush);
}

void RECTANGLE::WriteFile(ofstream &fo)
{
	type = 2;
	fo.write((char*)&type, sizeof(int));
	fo.write((char*)&left, sizeof(int));
	fo.write((char*)&top, sizeof(int));
	fo.write((char*)&right, sizeof(int));
	fo.write((char*)&bottom, sizeof(int));
	fo.write((char*)&color, sizeof(color));
}

void RECTANGLE::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }	
	switch (Type)
	{
	case 0:
	{
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	}break;
	case 1:
	{
		left = LOWORD(lParam);
		top = HIWORD(lParam);
	}break;
	case 2:
	{
		right = LOWORD(lParam);
		top = HIWORD(lParam);
	}break;
	case 3:
	{
		left = LOWORD(lParam);
		bottom = HIWORD(lParam);
	}break;
	case 4:
	{
		right = LOWORD(lParam);
		bottom = HIWORD(lParam);
	}break;
	}
	InvalidateRect(hWnd, NULL, true);
}


int RECTANGLE::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;
	int r;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r =RADIUS;
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}

bool RECTANGLE::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);

	if (bottom < top)
	{
		if ((clickpoint.x >= left && clickpoint.x <= right && clickpoint.y <= top && clickpoint.y >= bottom) || clickpoint.x <= left && clickpoint.x >= right && clickpoint.y <= top && clickpoint.y >= bottom)
			return true;
	}
	else
	{
		if ((clickpoint.x >= left && clickpoint.x <= right && clickpoint.y >= top && clickpoint.y <= bottom) || clickpoint.x <= left && clickpoint.x >= right && clickpoint.y >= top && clickpoint.y <= bottom)
			return true;
	}
	return false;
}

void RECTANGLE::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen = CreatePen(PS_DASHDOTDOT, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Rectangle(hdc, left, top,right,bottom);
}

int RECTANGLE::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;

	rec.left = rec.right = left;
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 1;

	rec.left = rec.right = right;
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 2;

	rec.left = rec.right = left;
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 3;

	rec.left = rec.right = right;
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 4;

	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}

void ELLIPSE::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 1, color);
	SelectObject(hdc, hPen);
	HBRUSH hBrush = CreateSolidBrush(_brush_color);
	MoveToEx(hdc, left, top, NULL);
	Ellipse(hdc,left, top, right, bottom);
	DeleteObject(hPen);
	DeleteObject(hBrush);
}

void ELLIPSE::WriteFile(ofstream &fo)
{
	type = 3;
	fo.write((char*)&type, sizeof(int));
	fo.write((char*)&left, sizeof(int));
	fo.write((char*)&top, sizeof(int));
	fo.write((char*)&right, sizeof(int));
	fo.write((char*)&bottom, sizeof(int));
	fo.write((char*)&color, sizeof(color));
}

void ELLIPSE::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{	
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }
	switch (Type)
	{
	case 0:	{		
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	}	break;
	case 1:	left = LOWORD(lParam);	break;
	case 2:	top = HIWORD(lParam);	break;
	case 3:	right = LOWORD(lParam);	break;
	case 4:	bottom = HIWORD(lParam);break;
	default: break;
	}
	InvalidateRect(hWnd, NULL, true);
}

int ELLIPSE::makeChange(HWND hWnd, LPARAM lParam)
{
	return GetResizeType(lParam);
}

int ELLIPSE::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;
	int r;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r =RADIUS;
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2)/* || func >= pow(r, 2) + 10*/)
		return true;
	return false;
}

bool ELLIPSE::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;
	int r;

	a = abs(left + ((right - left) / 2));
	b = abs(top + (bottom - top) / 2);
	r = abs((right - left) / 2);
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}

void ELLIPSE::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen;
	
	pen = CreatePen(PS_DASHDOTDOT, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Ellipse(hdc, abs(left + ((right - left) / 2)) -RADIUS, top -RADIUS, abs(left + ((right - left) / 2)) +RADIUS, top +RADIUS);
	Ellipse(hdc, abs(left + ((right - left) / 2)) -RADIUS, bottom -RADIUS, abs(left + ((right - left) / 2)) +RADIUS, bottom +RADIUS);
	Ellipse(hdc, left -RADIUS, abs(top + (bottom - top) / 2) -RADIUS, left +RADIUS, abs(top + (bottom - top) / 2) +RADIUS);
	Ellipse(hdc, right -RADIUS, abs(top + (bottom - top) / 2) -RADIUS, right +RADIUS, abs(top + (bottom - top) / 2) +RADIUS);
}

int ELLIPSE::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;

	rec.left = rec.right = left;
	rec.top = rec.bottom = abs(top + (bottom - top) / 2);
	if (isValidResize(lParam, rec))
		return 1;

	rec.left = rec.right = abs(left + ((right - left) / 2));
	rec.top = rec.bottom = top;
	if (isValidResize(lParam, rec))
		return 2;

	rec.left = rec.right = right;
	rec.top = rec.bottom = abs(top + (bottom - top) / 2);
	if (isValidResize(lParam, rec))
		return 3;

	rec.left = rec.right = abs(left + ((right - left) / 2));
	rec.top = rec.bottom = bottom;
	if (isValidResize(lParam, rec))
		return 4;

	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}

void TEXT::Draw(HDC hdc)
{
	HFONT hFont = CreateFontIndirect(&LFont);
	HBRUSH hBrush = CreateSolidBrush(_brush_color);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, color);
	OutputDebugString(str);	
	TextOut(hdc, left, top, str, wcsnlen(str,strlen));
	DeleteObject(hFont);
	DeleteObject(hBrush);
}

void TEXT::WriteFile(ofstream &fo)
{
	type = 4;
	int sizes =sizeof(WCHAR)* strlen;
	fo.write((char*)&type, sizeof(int));
	fo.write((char*)&left, sizeof(int));
	fo.write((char*)&top, sizeof(int));
	fo.write((char*)&right, sizeof(int));
	fo.write((char*)&bottom, sizeof(int));
	fo.write((char*)&color, sizeof(COLORREF));
	fo.write((char*)&LFont, sizeof(LOGFONT));
	fo.write((char*)&sizes, sizeof(int));
	fo.write((char*)&str, sizes);
}

void TEXT::Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int & Type)
{
	if (!(wParam & MK_LBUTTON)) { Type = -1; return; }
	switch (Type)
	{
	case 0:	{
		int x_move_lenght = LOWORD(lParam) - x_current;
		int y_move_lenght = HIWORD(lParam) - y_current;

		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);

		left += x_move_lenght;
		right += x_move_lenght;
		bottom += y_move_lenght;
		top += y_move_lenght;
	} break;
	default:break;
	}
	InvalidateRect(hWnd, NULL, true);
}

int TEXT::isValidResize(LPARAM lParam, RECT rec)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;
	int r;

	a = abs(rec.left + ((rec.right - rec.left) / 2));
	b = abs(rec.top + (rec.bottom - rec.top) / 2);
	r = abs((rec.right - rec.left) / 2);
	func = pow(clickpoint.x - a, 2) + pow(clickpoint.y - b, 2);
	if (func <= pow(r, 2) || func >= pow(r, 2) + 10)
		return true;
	return false;
}

bool TEXT::isSelect(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	int a, b, func;
	int r;

	if ((clickpoint.x >= left && clickpoint.x <= right && clickpoint.y >= top && clickpoint.y <= bottom) || clickpoint.x <= left && clickpoint.x >= right && clickpoint.y >= top && clickpoint.y <= bottom)
		return true;
	return false;
}

void TEXT::selected(HWND hWnd)
{
	HDC hdc = GetDC(hWnd);
	HPEN pen = CreatePen(PS_SOLID, 1, PEN_COLOR);
	SelectObject(hdc, pen);
	Ellipse(hdc, left -RADIUS, top -RADIUS, left +RADIUS, top +RADIUS);
	Ellipse(hdc, left -RADIUS, bottom -RADIUS, left +RADIUS, bottom +RADIUS);
	Ellipse(hdc, right -RADIUS, top -RADIUS, right +RADIUS, top +RADIUS);
	Ellipse(hdc, right -RADIUS, bottom -RADIUS, right +RADIUS, bottom +RADIUS);
}

int TEXT::GetResizeType(LPARAM lParam)
{
	POINTS clickpoint = MAKEPOINTS(lParam);
	RECT rec;

	if (isSelect(lParam))
	{
		x_current = LOWORD(lParam);
		y_current = HIWORD(lParam);
		return 0;
	}

	return -1;
}

