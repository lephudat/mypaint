// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include"Resource.h"
#include<CommCtrl.h>
#include<commdlg.h>
#include<PathCch.h>
#include<Shlwapi.h>


#define szChildWindowClass L"Child Window"
#define MAX_LOADSTRING 256
#define COLOR RGB(255, 255, 255)

#include"Draw.h"
// TODO: reference additional headers your program requires here
