#include"P1.h"
#include"stdafx.h"
void COLORCtrl()
{
	CHOOSECOLOR cc;
	COLORREF  Custom[16];
	DWORD rgbCurrent = RGB(255, 0, 0);

	ZeroMemory(&cc, sizeof(CHOOSECOLOR));

	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)Custom;
	cc.rgbResult = rgbCurrent;
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc))
	{
		//process when successed
	}
}
void FONTCtrl()
{
	CHOOSEFONT cf;
	LOGFONT lf;

	ZeroMemory(&cf, sizeof(CHOOSECOLOR));
	cf.hwndOwner = hWnd;
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.lpLogFont = &lf;

	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	if (ChooseFont(&cf))
	{
		//process when successed
	}
}
void InitialChildWnd(HWND hWnd, int type)
{
	CHILD_WND_DATA *wndData;
	wndData = new CHILD_WND_DATA;
	wndData->color = RGB(0, 0, 0);
	ZeroMemory(&(wndData->hFont), sizeof(HFONT));
	wndData->hFont.lfHeight = 28;
	wcscpy_s(wndData->hFont.lfFaceName, LF_FACESIZE, L"Arial");
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData))
		if (GetLastError() != 0)
			MessageBox(hWnd, L"Cannot init data pointer of window", L"Error", MB_OK);
}
void addUserToolbar(HWND hWnd)
{
	TBBUTTON tbButtons[] =
	{

		{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 1, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 2, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 3, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 4, ID_DRAW_SELECTOBJECT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBar, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[0].iBitmap += idx;
	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;

	// add buttons to toolbar
	SendMessage(hToolBar, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);

}
void InitializeToolbar(HWND hWnd)
{
	InitCommonControls();

	TBBUTTON tbbutton[] =
	{
		{ STD_FILENEW,ID_FILE_NEW,TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0 },
	{ STD_FILEOPEN,ID_FILE_OPEN,TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0 },
	{ STD_FILESAVE,ID_FILE_SAVE,TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0 },
	};


	hToolBar = CreateToolbarEx(hWnd, WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS | TB_AUTOSIZE | TBSTYLE_FLAT,
		NULL,
		sizeof(tbbutton) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbbutton,
		sizeof(tbbutton) / sizeof(TBBUTTON),
		0,
		0,
		0,
		0,
		sizeof(TBBUTTON));
	addUserToolbar(hWnd);
}
void UpdateCheckMenu(HMENU hMenuDraw, int wmId, int &SelectedDrawMode)
{
	CheckMenuItem(hMenuDraw, SelectedDrawMode, MF_UNCHECKED | MF_BYCOMMAND);
	SelectedDrawMode = wmId;
	CheckMenuItem(hMenuDraw, SelectedDrawMode, MF_CHECKED | MF_BYCOMMAND);
}
void InitialMDIClientWindow(HMENU hMenuDraw, HMENU hMenuWindow, HWND hWnd)
{
	CheckMenuItem(hMenuDraw, SelectedDrawMode, MF_CHECKED | MF_BYCOMMAND);
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = hMenuWindow;
	ccs.idFirstChild = 50001;
	hWndMDIClient = CreateWindow(L"MDICLIENT", NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL
		, 0, 0, 0, 0,
		hWnd, NULL, hInst, (LPVOID)&ccs);
	ShowWindow(hWndMDIClient, SW_SHOW);
}
void FileNewMenuHandle(WCHAR notify[], int &counter)
{
	counter++;
	wsprintf(notify, L"Noname-%d.drw", counter);
	MDICREATESTRUCT mdi;
	mdi.hOwner = hInst;
	mdi.szClass = szChildWindowClass;
	mdi.szTitle = notify;
	mdi.lParam = 0;
	mdi.style = 0;
	mdi.cx = CW_USEDEFAULT;
	mdi.cy = CW_USEDEFAULT;
	mdi.x = CW_USEDEFAULT;
	mdi.y = CW_USEDEFAULT;
	SendMessage(hWndMDIClient, WM_MDICREATE, 0L, (LPARAM)(LPMDICREATESTRUCT)&mdi);
}