#pragma once

LRESULT CALLBACK MDICloseProc(HWND hWndChild, LPARAM lParam)
{
	SendMessage(hWndMDIClient, WM_MDIDESTROY, (WPARAM)hWndChild, 0L);
	return 1;
}
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

class Shape
{
private:
	int x1;
	int y1;
	int x2;
	int y2;

public:
	virtual void Redraw();
	virtual void Save();
	virtual void GetCoordinate();
};


class Line :public Shape
{
	void Redraw();
	void Save();
	void GetCoordinate();
};
class Rectangle :public Shape
{
	void Redraw();
	void Save();
	void GetCoordinate();
};
class Ellipse :public Shape
{
	void Redraw();
	void Save();
	void GetCoordinate();
};
class Text :public Shape
{
	void Redraw();
	void Save();
	void GetCoordinate();
};
class SelectObject :public Shape
{
	void Redraw();
	void Save();
	void GetCoordinate();
};
