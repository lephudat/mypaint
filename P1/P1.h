extern HWND hWndMDIClient;
extern HWND hWndChild;
int SelectedDrawMode = ID_DRAW_LINE;

HWND hToolBar;
HWND hWnd = NULL;
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name


ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

bool check(WCHAR a[], WCHAR b[]);

void doWM_CLOSE(HWND hWnd);

void doWM_DESTROY(HWND hWnd);

void doWM_SIZE(HWND hWnd, LPARAM lParam);

void doWM_PAINT(HWND hWnd);

void doWM_NOTIFY(HWND hWnd, LPARAM lParam);

void doWM_CREATE(HWND hWnd, HMENU hMenuDraw, HMENU hMenuEdit, HMENU hMenuWindow);

void doWM_COMMAND(HWND hWnd, WPARAM wParam, WCHAR *notify, int&counter, HMENU hMenuDraw);