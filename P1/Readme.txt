﻿-----------------------------------------------
Tên: Lê Phú Đạt
MSSV: 1653120
Lớp:16CLC2
-----------------------------------------------
Các yêu cầu hoàn thành:
 - Color và Font được lưu trữ riêng cho mỗi child window
 - Các object (line, rectangle, ellipse, text) được lưu trữ riêng cho mỗi child window.
 - Text được tạo ngay vị trí click mouse (up).
 - Paint lại được các OBJECT với mỗi loại child window tương ứng.
 - So sánh được với tên mặc định khi save. Tự động lưu khi child window đã được lưu.
 - Gán tên tile cho child window nếu mà child window đã được lưu.
 - Select được tất cả các object.
 - Move được tất cả các object.
 - Resize được tất cả  các OBJECT (trừ TEXT).

Các lưu ý khi chạy:
 - Khi tạo text, click 1 lần để nhập nội dung và click bất kì để hiển thị nội dung đó.
 - Build với debug X64

-----------------------------------------------
Project được viết trên Visual Studio 2017 Comm.
Debug x64
Windows SDK version: 10.0.17134.0