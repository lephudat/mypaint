﻿#pragma once
#include<vector>
#include<fstream>
#include<string>
using namespace std;

#ifndef DRAW_HEADER
#define DRAW_HEADER

#define MAX_STRING 128 
#define RADIUS 5
#define PEN_COLOR RGB(255, 0, 0)

class OBJECT
{
public:
	int x_current;
	int y_current;
	//----------
	//xác định loại object
	//1.LINE
	//2.RECTANGLE
	//3.ELLIPSE
	//4.TEXT
	//----------
	int type;
	int left;
	int top;
	int right;
	int bottom;

	COLORREF color, _brush_color = RGB(255, 255, 255);

	virtual void Draw(HDC hdc)=0;
	virtual void WriteFile(ofstream &fo)=0;

	virtual void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc)=0;
	virtual int isValidResize(LPARAM lParam, RECT rec)=0;
	virtual bool isSelect(LPARAM lParam)=0;
	virtual void selected(HWND hWnd)=0;
	virtual int GetResizeType(LPARAM lParam)=0;
	
};
class LINE :public OBJECT
{
	
public:	
	void Draw(HDC hdc);
	void WriteFile(ofstream &fo);

	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class RECTANGLE :public OBJECT
{
public:
	void Draw(HDC hdc);
	void WriteFile(ofstream &fo);

	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class ELLIPSE :public OBJECT
{
public:
	void Draw(HDC hdc);
	void WriteFile(ofstream &fo);

	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int makeChange(HWND hWnd, LPARAM lParam);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
};
class TEXT :public OBJECT
{
public:
	int strlen;
	WCHAR str[MAX_STRING];
	LOGFONT LFont;
	void Draw(HDC hdc);
	void WriteFile(ofstream &fo);

	void Resize(HWND hWnd, LPARAM lParam, WPARAM wParam, int& loc);
	int isValidResize(LPARAM lParam, RECT rec);
	bool isSelect(LPARAM lParam);
	void selected(HWND hWnd);
	int GetResizeType(LPARAM lParam);
	
};
//Data structurer dành cho cửa sổ window
struct CHILD_WND_DATA
{
	//Lưu lại đường dẫn của file save
	WCHAR file[1024];
	//Lưu lại title cửa window
	WCHAR Filetitle[255];
	bool isSaved = false, isOpended = false;
	int stt;

	//lưu lại giá trị selected index của mảng vector
	int _indexSelected;
	//lưu lại front structurer của window
	LOGFONT hFont;
	//lưu lại color structurer của window
	COLORREF color;
	//lưu lại danh sách các hình ảnh đã vẽ
	vector<OBJECT*> objects;
};

#endif // !DRAW_HEADER
