﻿#include"stdafx.h"
#include"ClipboardHandle.h"

//-------------
//Lưu vết loại hình được chọn để copy.
//Nếu không có loại nào lưu thì set -1
//-------------
extern int typeSelected;

//khai báo định danh cho clipboard
WCHAR szLINEFORMAT[] = L"lephudat.98/P3/LINE";
WCHAR szRECTANGLEFORMAT[] = L"lephudat.98/P3/RECTANGLE";
WCHAR szELLIPSEFORMAT[] = L"lephudat.98/P3/ELLIPSE";
WCHAR szTEXTFORMAT[] = L"lephudat.98/P3/TEXT";


void doCopy_CB(HWND hWnd) {
	WCHAR MSG[MAX_LOADSTRING];
	if (OpenClipboard(hWnd)) {
		EmptyClipboard();
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		if (!wndData->objects.empty() && wndData->_indexSelected != -1) {
			if (wndData) {
				wsprintf(MSG, L"COPY MODE:\nloại object %d, số phần tử trong mảng %d, Tên HWND %s", typeSelected, wndData->objects.size(), wndData->Filetitle);
				OutputDebugString(MSG);
				switch (typeSelected)
				{
				case 1: {
					int nLineFormatID = RegisterClipboardFormat(szLINEFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(LINE));
					LINE * p = (LINE *)GlobalLock(hgbMen);
					p->type = typeSelected;
					p->bottom = wndData->objects[wndData->_indexSelected]->bottom;
					p->color = wndData->objects[wndData->_indexSelected]->color;
					p->left = wndData->objects[wndData->_indexSelected]->left;
					p->right = wndData->objects[wndData->_indexSelected]->right;
					p->top = wndData->objects[wndData->_indexSelected]->top;
					SetClipboardData(nLineFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				case 2: {
					RECTANGLE * tmp = (RECTANGLE *)wndData->objects[wndData->_indexSelected];
					int nRECTANGLEFormatID = RegisterClipboardFormat(szRECTANGLEFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(RECTANGLE));
					RECTANGLE * p = (RECTANGLE *)GlobalLock(hgbMen);
					p->type = tmp->type;
					p->bottom = tmp->bottom;
					p->color = tmp->color;
					p->left = tmp->left;
					p->right = tmp->right;
					p->top = tmp->top;
					SetClipboardData(nRECTANGLEFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				case 3: {
					int nELLIPSEFormatID = RegisterClipboardFormat(szELLIPSEFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(ELLIPSE));
					ELLIPSE * p = (ELLIPSE *)GlobalLock(hgbMen);
					p->type = p->type;
					p->bottom = wndData->objects[wndData->_indexSelected]->bottom;
					p->color = wndData->objects[wndData->_indexSelected]->color;
					p->left = wndData->objects[wndData->_indexSelected]->left;
					p->right = wndData->objects[wndData->_indexSelected]->right;
					p->top = wndData->objects[wndData->_indexSelected]->top;
					SetClipboardData(nELLIPSEFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				case 4: {
					TEXT * tmp = (TEXT *)wndData->objects[wndData->_indexSelected];
					int nTEXTFormatID = RegisterClipboardFormat(szTEXTFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(TEXT));
					TEXT * p = (TEXT *)GlobalLock(hgbMen);
					p->type = tmp->type;
					p->bottom = tmp->bottom;
					p->color = tmp->color;
					p->left = tmp->left;
					p->right = tmp->right;
					p->top = tmp->top;
					p->LFont = tmp->LFont;
					wsprintf(p->str, tmp->str);
					p->strlen = tmp->strlen;
					SetClipboardData(nTEXTFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				}
			}
		}
		CloseClipboard();
	}
}

void doPaste_CB(HWND hWnd) {
	if (OpenClipboard(hWnd)) {
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		int nTriFormatID = RegisterClipboardFormat(szLINEFORMAT);
		int nTriFormatID1 = RegisterClipboardFormat(szRECTANGLEFORMAT);
		int nTriFormatID2 = RegisterClipboardFormat(szELLIPSEFORMAT);
		int nTriFormatID4 = RegisterClipboardFormat(szTEXTFORMAT);
		HANDLE hText = GetClipboardData(CF_TEXT);
		HGLOBAL hgbMen4 = GetClipboardData(nTriFormatID4);
		HGLOBAL hgbMen2 = GetClipboardData(nTriFormatID2);
		HGLOBAL hgbMen = GetClipboardData(nTriFormatID);
		HGLOBAL hgbMen1 = GetClipboardData(nTriFormatID1);
		if (hgbMen)
		{
			LINE *tmp = (LINE *)GlobalLock(hgbMen);
			LINE *p = new LINE;
			p->type = tmp->type;
			p->bottom = tmp->bottom;
			p->color = tmp->color;
			p->left = tmp->left;
			p->right = tmp->right;
			p->top = tmp->top;

			wndData->objects.push_back(p);
			SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
			InvalidateRect(hWnd, NULL, true);
			GlobalUnlock(hgbMen);
		}
		else if (hgbMen1)
		{
			RECTANGLE *tmp = (RECTANGLE *)GlobalLock(hgbMen1);
			RECTANGLE *p = new RECTANGLE;
			p->type = tmp->type;
			p->bottom = tmp->bottom;
			p->color = tmp->color;
			p->left = tmp->left;
			p->right = tmp->right;
			p->top = tmp->top;

			wndData->objects.push_back(p);
			SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
			InvalidateRect(hWnd, NULL, true);
			GlobalUnlock(hgbMen);
		}
		else if (hgbMen2)
		{
			ELLIPSE *tmp = (ELLIPSE *)GlobalLock(hgbMen2);
			ELLIPSE *p = new ELLIPSE;
			p->type = tmp->type;
			p->bottom = tmp->bottom;
			p->color = tmp->color;
			p->left = tmp->left;
			p->right = tmp->right;
			p->top = tmp->top;

			wndData->objects.push_back(p);
			SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
			InvalidateRect(hWnd, NULL, true);
			GlobalUnlock(hgbMen);
		}
		else if (hgbMen4)
		{
			TEXT *tmp = (TEXT *)GlobalLock(hgbMen4);
			TEXT *p = new TEXT;
			p->type = tmp->type;
			p->bottom = tmp->bottom;
			p->color = tmp->color;
			p->left = tmp->left;
			p->right = tmp->right;
			p->top = tmp->top;
			p->LFont = tmp->LFont;
			wsprintf(p->str, tmp->str);
			p->strlen = tmp->strlen;

			wndData->objects.push_back(p);
			SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
			InvalidateRect(hWnd, NULL, true);
			GlobalUnlock(hgbMen);
		}
		else if (hText) {
			TEXT *p = new TEXT;
			p->type = 4;

			p->left = 0;
			p->top = 0;
			char* _ansitextData1 = static_cast<char*>(GlobalLock(hText));
			size_t convertedChars = 0;
			WCHAR *_ansitextData = new WCHAR[strlen(_ansitextData1)];
			mbstowcs_s(&convertedChars, _ansitextData, strlen(_ansitextData1) + 1, _ansitextData1, _TRUNCATE);
			wsprintf(p->str, _ansitextData);
			HDC hdc = GetDC(hWnd);
			HFONT hFont = CreateFontIndirect(&wndData->hFont);
			SelectObject(hdc, hFont);
			SetTextColor(hdc, wndData->color);
			TextOut(hdc, p->left, p->top, p->str, lstrlen(p->str));
			SIZE size;
			GetTextExtentPoint32(hdc, p->str, lstrlen(p->str), &size);
			p->right = p->left + size.cx;
			p->bottom = p->top + size.cy;
			p->strlen = wcslen(p->str);
			p->LFont = (LOGFONT&)wndData->hFont;
			p->color = wndData->color;
			wndData->objects.push_back(p);
			SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
			InvalidateRect(hWnd, NULL, true);
			GlobalUnlock(hText);
			ReleaseDC(hWnd, hdc);
		}
		CloseClipboard();
	}
}

void doCut_CB(HWND hWnd) {
	if (OpenClipboard(hWnd)) {
		EmptyClipboard();
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
		if (!wndData->objects.empty() && wndData->_indexSelected != -1) {
			if (wndData) {
				switch (typeSelected)
				{
				case 1: {
					int nLineFormatID = RegisterClipboardFormat(szLINEFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(LINE));
					LINE * p = (LINE *)GlobalLock(hgbMen);
					p->type = wndData->objects[wndData->_indexSelected]->type;
					p->bottom = wndData->objects[wndData->_indexSelected]->bottom;
					p->color = wndData->objects[wndData->_indexSelected]->color;
					p->left = wndData->objects[wndData->_indexSelected]->left;
					p->right = wndData->objects[wndData->_indexSelected]->right;
					p->top = wndData->objects[wndData->_indexSelected]->top;
					SetClipboardData(nLineFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				case 2: {
					RECTANGLE * tmp = (RECTANGLE *)wndData->objects[wndData->_indexSelected];
					int nRECTANGLEFormatID = RegisterClipboardFormat(szRECTANGLEFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(RECTANGLE));
					RECTANGLE * p = (RECTANGLE *)GlobalLock(hgbMen);
					p->type = wndData->objects[wndData->_indexSelected]->type;
					p->bottom = tmp->bottom;
					p->color = tmp->color;
					p->left = tmp->left;
					p->right = tmp->right;
					p->top = tmp->top;
					SetClipboardData(nRECTANGLEFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				case 3: {
					int nELLIPSEFormatID = RegisterClipboardFormat(szELLIPSEFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(ELLIPSE));
					ELLIPSE * p = (ELLIPSE *)GlobalLock(hgbMen);
					p->type = wndData->objects[wndData->_indexSelected]->type;
					p->bottom = wndData->objects[wndData->_indexSelected]->bottom;
					p->color = wndData->objects[wndData->_indexSelected]->color;
					p->left = wndData->objects[wndData->_indexSelected]->left;
					p->right = wndData->objects[wndData->_indexSelected]->right;
					p->top = wndData->objects[wndData->_indexSelected]->top;
					SetClipboardData(nELLIPSEFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				case 4: {
					TEXT * tmp = (TEXT *)wndData->objects[wndData->_indexSelected];
					int nTEXTFormatID = RegisterClipboardFormat(szTEXTFORMAT);
					HANDLE hgbMen = GlobalAlloc(GHND, sizeof(TEXT));
					TEXT * p = (TEXT *)GlobalLock(hgbMen);
					p->type = wndData->objects[wndData->_indexSelected]->type;
					p->bottom = tmp->bottom;
					p->color = tmp->color;
					p->left = tmp->left;
					p->right = tmp->right;
					p->top = tmp->top;
					p->LFont = tmp->LFont;
					wsprintf(p->str, tmp->str);
					p->strlen = tmp->strlen;
					SetClipboardData(nTEXTFormatID, hgbMen);
					GlobalUnlock(hgbMen);
					break;
				}
				}
				if (typeSelected >= 1 && typeSelected <= 4) {
					vector<OBJECT*> objects;
					for (int i = 0; i < wndData->objects.size(); i++)
					{
						if (i != wndData->_indexSelected)
							objects.push_back(wndData->objects[i]);
						else
						{
							delete[]wndData->objects[i];
						}
					}
					wndData->objects = objects;
					wndData->_indexSelected = -1;
					SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
					InvalidateRect(hWnd, NULL, true);
				}
			}
		}
		CloseClipboard();
	}
}

void doDelete_CB(HWND hWnd) {
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (wndData->_indexSelected != -1) {
		if (wndData)
			if (wndData->objects.size() != 0) {

				vector<OBJECT*> objects;
				for (int i = 0; i < wndData->objects.size(); i++)
				{
					if (i != wndData->_indexSelected)
						objects.push_back(wndData->objects[i]);
					else
					{
						delete[]wndData->objects[i];
					}
				}
				wndData->objects = objects;
				wndData->_indexSelected = -1;
				SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData);
				InvalidateRect(hWnd, NULL, true);
			}
	}
}