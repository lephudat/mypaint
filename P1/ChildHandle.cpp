﻿#include"stdafx.h"
#include"ChildHandle.h"
#include"ClipboardHandle.h"

extern HINSTANCE hInst;
extern HWND hWnd;
extern int SelectedDrawMode;
extern HWND hToolBar;

WCHAR Notify[MAX_LOADSTRING];
HWND hWndChild = NULL;
HWND hWndMDIClient = NULL;
int typeSelected = -1;
bool flag =true;
//Lưu vết phần tử đã chọn trong danh sách các OBJECT
int type1 = -1;

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int type, x1, x2, y1, y2, x_saved, y_saved;
	static bool inputted = false;
	static HWND hText;
	static WCHAR FileTile[MAX_LOADSTRING];
	static	int count = 0;
	switch (message)
	{
	case WM_COMMAND:
	{
		break;
	}
	case WM_CREATE: {
		type1 = -1;
		count++;
		InitialChildWnd(hWnd, count);
		break;
	}
	case WM_MOUSEMOVE: {
		if (SelectedDrawMode == ID_DRAW_SELECTOBJECT)
		{
			CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
			if (type1 != -1 && !wndData->objects.empty()&& wndData->_indexSelected!=-1)
			{
				_Edit_OnMouseMove(hWnd, wParam, lParam, wndData->objects[wndData->_indexSelected], type1);
				wndData->objects[wndData->_indexSelected]->selected(hWnd);
			}
		}
		else
		{
			_Draw_OnMouseMove(hWnd, wParam, lParam, x1, x2, y1, y2);
		}

		break;
	}
	case WM_LBUTTONDOWN: {
		//OutputDebugString(L"Down\n");
		if (SelectedDrawMode == ID_DRAW_SELECTOBJECT)
		{
			_Edit_OnLButtonDown(hWnd, lParam, type1);
		}
		else
		{
			_Draw_OnLButtonDown(hWnd, wParam, lParam, x1, x2, y1, y2);
		}
		break;
	}
	case WM_LBUTTONUP: {
		if (SelectedDrawMode == ID_DRAW_SELECTOBJECT)
		{

		}
		else
		{
			_Draw_OnLButtonUp(hWnd, wParam, lParam, x1, x2, y1, y2, inputted, hText, x_saved, y_saved);
		}
		break;
	}
	case WM_PAINT: {
		OnWmPaint(hWnd);
	}
	case WM_MDIACTIVATE:
	{
		hWndChild = hWnd;
		break;
	}
	case WM_SIZE:
	{
		InvalidateRect(hWnd, NULL, true);
		break;
	}
	case WM_MDIDESTROY:
	{
		hWndChild = NULL;
		//OutputDebugString(L"WM_CLOSE_MDI.....\n");
		break;
	}
	case WM_CLOSE:
	{		
		MDICloseProc(hWnd, lParam);
		break;
	}
	}
	return DefMDIChildProc(hWnd, message,
		wParam, lParam);
}

ATOM                MyRegisterChildClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = ChildWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_P1);
	wcex.lpszClassName = szChildWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

LRESULT CALLBACK MDICloseProc(HWND hWnd, LPARAM lParam)
{
	SendMessage(hWndMDIClient, WM_MDIDESTROY, (WPARAM)hWnd, 0L);
	return 1;
}

OPENFILENAME SaveFileDialog(HWND hwnd)
{
	OPENFILENAME ofn;       // common dialog box structure
	WCHAR szFile[MAX_LOADSTRING];       // buffer for file name
	HANDLE hf;              // file handle
	WCHAR szFiles[MAX_LOADSTRING];
	int i;
	// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	GetWindowText(hWndChild, szFiles, MAX_LOADSTRING);
	for (i = 0; i < sizeof(szFiles); i++)
	{
		if (szFiles[i] == '.')
		{
			bool isdot = false;
			for (int j = i + 1; j<sizeof(szFiles); j++)
				if (szFiles[i] == '.')
				{
					isdot = true;
					break;
				}
			if (isdot)
				break;
		}
		ofn.lpstrFile[i] = szFiles[i];
	}
	ofn.lpstrFile[i] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"*.drw\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// Display the Open dialog box. 

	if (GetSaveFileName(&ofn) == TRUE)
	{
		std::wstring path(ofn.lpstrFile);
		path.append(L".drw");
		wsprintf(ofn.lpstrFile, path.c_str());
	}
	return ofn;
}

OPENFILENAME OpenFileDialog(HWND hwnd)
{
	OPENFILENAME ofn;       // common dialog box structure
	WCHAR szFile[MAX_LOADSTRING];       // buffer for file name
	HANDLE hf;              // file handle

							// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;

	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Draw(*.drw)\0*.drw\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// Display the Open dialog box. 

	if (GetOpenFileName(&ofn) == TRUE)
	{
		MessageBox(hWnd, ofn.lpstrFile, L"Notification", MB_OK);
		int n = 0;
		int type;
		ifstream fi;
		fi.open(ofn.lpstrFile, ios::binary | ios::in);
		if (!fi)
			OutputDebugString(L"\nmo file ko dc\n");
		else
		{

			
			CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)VirtualAlloc(NULL, sizeof(CHILD_WND_DATA), MEM_COMMIT, PAGE_READWRITE);
			int tmp1;
			wndData->isOpended = true;
			fi.read((char*)&tmp1, sizeof(int));
			fi.read((char*)&wndData->file, tmp1);
			//OutputDebugString(wndData->file);
			wsprintf(Notify, L"Open n %d\n", n);
			wcscpy_s(wndData->Filetitle, PathFindFileName(wndData->file));
			SetWindowText(hWndChild, wndData->Filetitle);

			fi.read((char*)&n, sizeof(int));
			wsprintf(Notify, L"Open n %d\n", n);
			//OutputDebugString(Notify);
			fi.read((char*)&wndData->color, sizeof(wndData->color));
			fi.read((char*)&wndData->hFont, sizeof(wndData->hFont));

			for (int i = 0; i < n; i++)
			{
				fi.read((char*)&type, sizeof(int));
				
				wsprintf(Notify, L"Open type %d\n", type);
				//OutputDebugString(Notify);
				switch (type)
				{
				case 1:
				{
					LINE*l = new LINE;
					l->type = type;
					fi.read((char*)&l->left, sizeof(int));
					fi.read((char*)&l->top, sizeof(int));
					fi.read((char*)&l->right, sizeof(int));
					fi.read((char*)&l->bottom, sizeof(int));
					fi.read((char*)&l->color, sizeof(COLORREF));
					wndData->objects.push_back(l);
					break;
				}
				case 2:
				{
					RECTANGLE*l = new RECTANGLE;
					l->type = type;
					fi.read((char*)&l->left, sizeof(int));
					fi.read((char*)&l->top, sizeof(int));
					fi.read((char*)&l->right, sizeof(int));
					fi.read((char*)&l->bottom, sizeof(int));
					fi.read((char*)&l->color, sizeof(l->color));
					wndData->objects.push_back(l);
					break;
				}
				case 3:
				{
					ELLIPSE*l = new ELLIPSE;
					l->type = type;
					fi.read((char*)&l->left, sizeof(int));
					fi.read((char*)&l->top, sizeof(int));
					fi.read((char*)&l->right, sizeof(int));
					fi.read((char*)&l->bottom, sizeof(int));
					fi.read((char*)&l->color, sizeof(l->color));
					wndData->objects.push_back(l);
					break;
				}
				case 4:
				{
					TEXT*l = new TEXT;
					l->type = type;
					fi.read((char*)&l->left, sizeof(int));
					fi.read((char*)&l->top, sizeof(int));
					fi.read((char*)&l->right, sizeof(int));
					fi.read((char*)&l->bottom, sizeof(int));
					fi.read((char*)&l->color, sizeof(COLORREF));
					fi.read((char*)&l->LFont, sizeof(LOGFONT));
					int sizes;
					fi.read((char*)&sizes, sizeof(int));
					fi.read((char*)&l->str, sizes);
					l->strlen = sizes / sizeof(WCHAR);
					//OutputDebugString(l->str);
					wndData->objects.push_back(l);
					break;
				}
				default:
					break;
				}
			}
			fi.close();
			SetWindowLongPtr(hWndChild, 0, (LONG_PTR)wndData);
		}
	}
	return ofn;
}

CHOOSECOLOR COLORCtrl()
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
	CHOOSECOLOR cc;
	COLORREF  Custom[16];
	DWORD rgbCurrent = wndData->color;

	ZeroMemory(&cc, sizeof(CHOOSECOLOR));

	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)Custom;
	cc.rgbResult = rgbCurrent;
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	if (ChooseColor(&cc))
	{
		return cc;
	}
	return cc;
}
CHOOSEFONT FONTCtrl()
{
	CHOOSEFONT cf;
	LOGFONT lf;

	ZeroMemory(&cf, sizeof(CHOOSECOLOR));
	cf.hwndOwner = hWnd;
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.lpLogFont = &lf;

	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	if (ChooseFont(&cf))
	{
		return cf;
	}
	return cf;
}
void InitialChildWnd(HWND hWnd, int type)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)VirtualAlloc(NULL, sizeof(CHILD_WND_DATA), MEM_COMMIT, PAGE_READWRITE);
	wndData->stt = type;
	wndData->_indexSelected = -1;
	GetWindowText(hWnd, wndData->Filetitle, 256);
	//OutputDebugString(wndData->Filetitle);
	ZeroMemory(&(wndData->hFont), sizeof(LOGFONT));
	wndData->hFont.lfHeight = 28;
	wcscpy_s(wndData->hFont.lfFaceName, LF_FACESIZE, L"Arial");
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData))
		if (GetLastError() != 0)
			MessageBox(hWnd, L"Cannot init data pointer of window", L"Error", MB_OK);
}
void addUserToolbar(HWND hWnd)
{
	TBBUTTON tbButtons[] =
	{

		{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 1, ID_DRAW_ELLIPSE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 2, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 3, ID_DRAW_TEXT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ 4, ID_DRAW_SELECTOBJECT,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBar, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbButtons[0].iBitmap += idx;
	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;

	// add buttons to toolbar
	SendMessage(hToolBar, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);

}
void InitializeToolbar(HWND hWnd)
{
	InitCommonControls();

	TBBUTTON tbbutton[] =
	{
		{ STD_FILENEW,ID_FILE_NEW,TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0 },
	{ STD_FILEOPEN,ID_FILE_OPEN,TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0 },
	{ STD_FILESAVE,ID_FILE_SAVE,TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0 },
	};


	hToolBar = CreateToolbarEx(hWnd, WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS | TB_AUTOSIZE | TBSTYLE_FLAT,
		NULL,
		sizeof(tbbutton) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbbutton,
		sizeof(tbbutton) / sizeof(TBBUTTON),
		0,
		0,
		0,
		0,
		sizeof(TBBUTTON));
	addUserToolbar(hWnd);
}
void UpdateCheckMenu(HMENU hMenuDraw, int wmId, int &SelectedDrawMode)
{
	EnableMenuItem(GetMenu(hWnd), 1, MF_ENABLED | MF_BYPOSITION);
	CheckMenuItem(hMenuDraw, SelectedDrawMode, MF_UNCHECKED | MF_BYCOMMAND);
	SelectedDrawMode = wmId;
	CheckMenuItem(hMenuDraw, SelectedDrawMode, MF_CHECKED | MF_BYCOMMAND);
}
void InitialMDIClientWindow(HMENU hMenuDraw, HMENU hMenuEdit, HMENU hMenuWindow, HWND hWnd)
{
	CheckMenuItem(hMenuDraw, SelectedDrawMode, MF_CHECKED | MF_BYCOMMAND);
	EnableMenuItem(GetMenu(hWnd), 1, MF_GRAYED | MF_BYPOSITION);
	EnableMenuItem(hMenuDraw, ID_DRAW_FONT, MF_GRAYED | MF_BYCOMMAND);
	EnableMenuItem(hMenuDraw, ID_DRAW_COLOR, MF_GRAYED | MF_BYCOMMAND);
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = hMenuWindow;
	ccs.idFirstChild = 50001;
	hWndMDIClient = CreateWindow(L"MDICLIENT", NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL
		, 0, 0, 0, 0,
		hWnd, NULL, hInst, (LPVOID)&ccs);
	ShowWindow(hWndMDIClient, SW_SHOW);
	DrawMenuBar(hWnd);
}
HWND FileNewMenuHandle(WCHAR notify[], int &counter)
{
	counter++;
	wsprintf(notify, L"Noname-%d.drw", counter);
	MDICREATESTRUCT mdi;
	mdi.hOwner = hInst;
	mdi.szClass = szChildWindowClass;
	mdi.szTitle = notify;
	mdi.lParam = 0;
	mdi.style = 0;
	mdi.cx = CW_USEDEFAULT;
	mdi.cy = CW_USEDEFAULT;
	mdi.x = CW_USEDEFAULT;
	mdi.y = CW_USEDEFAULT;
	return (HWND)SendMessage(hWndMDIClient, WM_MDICREATE, 0L, (LPARAM)(LPMDICREATESTRUCT)&mdi);

}
HWND InputText(HWND hWnd, int left, int top)
{
	HWND EditBox = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, left, top,
		500, 25, hWnd, nullptr, hInst, nullptr);
	ShowWindow(EditBox, SW_NORMAL);
	SetFocus(EditBox);
	return EditBox;
}
void _Draw_OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2)
{
	if ((wParam&MK_LBUTTON) == MK_LBUTTON)
	{
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
		//OutputDebugString(L"child WM_MOVE\n");
		HDC hdc;
		hdc = GetDC(hWnd);
		HPEN hPen = CreatePen(PS_SOLID, 1, wndData->color);
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_NOTXORPEN);
		MoveToEx(hdc, x1, y1, NULL);
		switch (SelectedDrawMode)
		{
		case ID_DRAW_LINE:
			LineTo(hdc, x2, y2);
			break;
		case ID_DRAW_ELLIPSE:
			Ellipse(hdc, x1, y1, x2, y2);
			break;
		case ID_DRAW_RECTANGLE:
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		default:
			break;
		}
		x2 = LOWORD(lParam);
		y2 = HIWORD(lParam);
		MoveToEx(hdc, x1, y1, NULL);
		switch (SelectedDrawMode)
		{
		case ID_DRAW_LINE:
			LineTo(hdc, x2, y2);
			break;
		case ID_DRAW_ELLIPSE:
			Ellipse(hdc, x1, y1, x2, y2);

			break;
		case ID_DRAW_RECTANGLE:
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		default:
			break;

		}

		ReleaseDC(hWnd, hdc);
		DeleteObject(hPen);
	}
}
void _Edit_OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, OBJECT*obj, int type)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
	if (!wndData->objects.empty()&&wndData->_indexSelected != -1)
		obj->Resize(hWnd, lParam, wParam, type);
}

void _Draw_OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2)
{
	SetFocus(hWnd);
	x1 = x2 = LOWORD(lParam);
	y1 = y2 = HIWORD(lParam);
	OutputDebugString(L"Child window WM_LBUTTONDOWN\n");
}

void _Edit_OnLButtonDown(HWND hWnd, LPARAM lParam, int &type)
{
	CHILD_WND_DATA*p = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if(!p->objects.empty())
	{
		InvalidateRect(hWnd, NULL, true);
		for (int i = 0; i < p->objects.size(); i++)
		{
			if (p->objects.at(i) != NULL)
			{
				if (p->objects.at(i)->isSelect(lParam) == true)
				{

					WCHAR MSG[MAX_LOADSTRING];
					p->objects.at(i)->selected(hWnd);
					p->_indexSelected = i;
					wsprintf(MSG, L"SELECT: type %d, index %d\n", p->objects.at(i)->type, i);
					OutputDebugString(MSG);
					typeSelected = p->objects.at(i)->type;
					SetWindowLongPtr(hWndChild, 0, (LONG_PTR)p);
					type = p->objects.at(p->_indexSelected)->GetResizeType(lParam);
					break;
				}
			}
		}

		if (p->_indexSelected != -1)
		{
			type = p->objects.at(p->_indexSelected)->GetResizeType(lParam);
			if (type == -1)
			{
				RECT rec;
				GetClientRect(hWnd, &rec);
				InvalidateRect(hWnd, &rec, true);
				for (int i = 0; i < p->objects.size(); i++)
				{
					if (p->objects.at(i) != NULL)
					{
						if (p->objects.at(i)->isSelect(lParam) == true)
						{
							p->objects.at(i)->selected(hWnd);
							type = p->objects.at(i)->GetResizeType(lParam);
							return;
						}
					}
				}
			}
		}
	}
}

void _Draw_OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2, bool &inputted, HWND &hText
	, int &x_saved, int &y_saved)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL)
		MessageBox(hWnd, L"Error while getting data of window", L"Error", MB_OK);
	switch (SelectedDrawMode)
	{
	case ID_DRAW_LINE:
	{
		LINE * l = new LINE;
		l->type = 1;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		l->color = wndData->color;
		wndData->objects.push_back(l);
		break;
	}
	case ID_DRAW_RECTANGLE:
	{
		RECTANGLE * l = new RECTANGLE;
		l->type = 2;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		l->color = wndData->color;
		wndData->objects.push_back(l);
		break;
	}
	case ID_DRAW_ELLIPSE:
	{
		ELLIPSE * l = new ELLIPSE;
		l->type = 3;
		l->left = x1;
		l->top = y1;
		l->right = x2;
		l->bottom = y2;
		l->color = wndData->color;
		wndData->objects.push_back(l);
		break;
	}
	case ID_DRAW_TEXT:
	{
		if (inputted == false)
		{
			hText = InputText(hWnd, x1, y1);
			inputted = true;
			x_saved = x1;
			y_saved = y1;
		}
		else
		{
			ShowWindow(hText, SW_HIDE);
			WCHAR notify[MAX_LOADSTRING];
			HDC hdc = GetDC(hWnd);
			HFONT hFont = CreateFontIndirect(&wndData->hFont);
			SelectObject(hdc, hFont);
			SetTextColor(hdc, wndData->color);
			GetWindowText(hText, notify, MAX_LOADSTRING);
			TextOut(hdc, x_saved, y_saved, notify, wcsnlen(notify, MAX_STRING));
			SIZE size;
			GetTextExtentPoint32(hdc, notify, lstrlen(notify), &size);
			inputted = false;
			TEXT *t = new TEXT;
			t->type = 4;
			t->left = x_saved;
			t->top = y_saved;
			t->right = x_saved + size.cx;
			t->bottom = y_saved + size.cy;
			t->strlen = wcslen(notify);
			t->LFont = (LOGFONT&)wndData->hFont;
			t->color = wndData->color;
			wsprintf(t->str, notify);
			wndData->objects.push_back(t);
			DeleteObject(hFont);
			ReleaseDC(hWnd, hdc);

		}
		break;
	}
	default:
		break;
	}
	//OutputDebugString(L"Child window WM_LBUTTONUP\n");
}

void _Edit_OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2, bool &inputted, HWND &hText
	, int &x_saved, int &y_saved)
{

}

void OnWmPaint(HWND hWnd)
{	
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL)
		return;
	wsprintf(Notify, L"CHILD_WND_DATA size %d\n", wndData->objects.size());
	//OutputDebugString(Notify);
	for (int i = 0; i< wndData->objects.size(); i++)
		wndData->objects[i]->Draw(hdc);
	EndPaint(hWnd, &ps);
}

void doEditCopyMenu(HWND hWnd)
{
	doCopy_CB(hWndChild);
}

void doEditPasteMenu(HWND hWnd)
{
	doPaste_CB(hWndChild);
}

void doEditCutMenu(HWND hWnd)
{
	doCut_CB(hWndChild);
}

void doEditDeleteMenu(HWND hWnd)
{
	doDelete_CB(hWndChild);
}
