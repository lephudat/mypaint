// P1.cpp : Defines the entry point for the application.
//

#include"stdafx.h"
#include"P1.h"
#include"ChildHandle.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);
	MyRegisterChildClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;
    // Main message loop:

	while (GetMessage(&msg, (HWND)NULL, 0, 0) != 0)
	{
		if (!TranslateMDISysAccel(hWndMDIClient, &msg) &&
			!TranslateAccelerator(hWnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

	}
    return (int) msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static WCHAR notify[MAX_LOADSTRING];
	static int counter = 0;
	static HMENU hMenuEdit;
	hMenuEdit = GetSubMenu((GetMenu(hWnd)), 1);
	static HMENU hMenuDraw;
	hMenuDraw = GetSubMenu((GetMenu(hWnd)), 2);
	static HMENU hMenuWindow = GetSubMenu(GetMenu(hWnd), 3);

    switch (message)
    {
    case WM_COMMAND: {
		doWM_COMMAND(hWnd, wParam, notify, counter, hMenuDraw);
		return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
        }
    case WM_PAINT: {
		doWM_PAINT(hWnd);        
        break;
	}
	case WM_CREATE: {
		doWM_CREATE(hWnd, hMenuDraw, hMenuEdit, hMenuWindow);
		break;
	}
	case WM_NOTIFY:	{
		doWM_NOTIFY(hWnd,lParam);
		return 0;
	}
	case WM_SIZE:
	{
		doWM_SIZE(hWnd, lParam);
		return 0;
	}
	case WM_CLOSE:
	{
		doWM_CLOSE(hWnd);
		break;
	}
	case WM_DESTROY:
	{
		doWM_DESTROY(hWnd);
		break;
	}	
	}
	return DefFrameProc(hWnd, hWndMDIClient, message, wParam, lParam);
}


ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_P1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

bool check(WCHAR a[], WCHAR b[])
{
	for (int i = 0; i < sizeof(a); i++)
		if (a[i] != b[i])
			return false;
	return true;
}

void doWM_CLOSE(HWND hWnd) {
	PostQuitMessage(0);
}

void doWM_DESTROY(HWND hWnd) {
	PostQuitMessage(0);
}

void doWM_SIZE(HWND hWnd,LPARAM lParam) {
	UINT h, w;
	h = HIWORD(lParam);
	w = LOWORD(lParam);
	MoveWindow(hToolBar, 0, 0, w, h, TRUE);
	MoveWindow(hWndMDIClient, 0, 25, w, h - 25, TRUE);
}

void doWM_PAINT(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void doWM_NOTIFY(HWND hWnd,LPARAM lParam) {
	LPTOOLTIPTEXT ToolTip;
	TCHAR szNotify[MAX_LOADSTRING];

	ToolTip = (LPTOOLTIPTEXT)lParam;
	if (ToolTip->hdr.code == TTN_NEEDTEXT)
	{
		LoadString(hInst, ToolTip->hdr.idFrom, szNotify, MAX_LOADSTRING);
		ToolTip->lpszText = szNotify;
	}
}

void doWM_CREATE(HWND hWnd,HMENU hMenuDraw, HMENU hMenuEdit, HMENU hMenuWindow) {
	InitialMDIClientWindow(hMenuDraw,hMenuEdit,hMenuWindow,hWnd);
	InitializeToolbar(hWnd);
}

void doWM_COMMAND(HWND hWnd,WPARAM wParam, WCHAR *notify, int&counter,HMENU hMenuDraw) {
	int wmId = LOWORD(wParam);
	// Parse the menu selections:
	switch (wmId)
	{
	case ID_EDIT_COPY: {
		doEditCopyMenu(hWnd);
		OutputDebugString(L"Menu copy ne\n");
		break;
	}
	case ID_EDIT_CUT: {
		doEditCutMenu(hWnd);
		break;
	}
	case ID_EDIT_PASTE: {
		doEditPasteMenu(hWnd);
		break;
	}
	case ID_EDIT_DELETE: {
		doEditDeleteMenu(hWnd);
		break;
	}
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	case ID_DRAW_FONT:
	{
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
		wndData->hFont = *FONTCtrl().lpLogFont;
		SetWindowLongPtr(hWndChild, 0, (LONG_PTR)wndData);
		break;
	}
	case ID_DRAW_COLOR:
	{
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
		wndData->color = COLORCtrl().rgbResult;
		SetWindowLongPtr(hWndChild, 0, (LONG_PTR)wndData);
		break;
	}
	case ID_FILE_OPEN:
	{
		if (hWndChild == NULL)
			FileNewMenuHandle(notify, counter);
		MessageBox(hWnd, L"Bạn chọn hộp thoại Open", L"Notice", MB_OK);
		OpenFileDialog(hWnd);
		InvalidateRect(hWndChild, NULL, true);
		break;
	}
	case ID_FILE_SAVE:
	{
		MessageBox(hWnd, L"Bạn chọn hộp thoại Save", L"Notice", MB_OK);
		if (hWndChild != NULL)
		{
			CHILD_WND_DATA *wndData = (CHILD_WND_DATA*)GetWindowLongPtr(hWndChild, 0);
			WCHAR WndName[256];
			GetWindowText(hWndChild, WndName, 256);
			if (check(WndName, wndData->Filetitle) == true
				&& !wndData->isSaved && !wndData->isOpended)
			{
				wndData->isSaved = true;
				wcscpy_s(wndData->file, SaveFileDialog(hWnd).lpstrFile);
				ofstream fo;
				fo.open(wndData->file, ios::binary | ios::out);
				int tmp1, tmp = wndData->objects.size();

				WCHAR ChildTitle[256];
				wsprintf(ChildTitle, PathFindFileName(wndData->file));
				tmp1 = sizeof(WCHAR) *wcslen(wndData->file);
				fo.write((char*)&tmp1, sizeof(int));
				fo.write((char*)&wndData->file, tmp1);
				fo.write((char*)&tmp, sizeof(int));
				fo.write((char*)&wndData->color, sizeof(wndData->color));
				fo.write((char*)&wndData->hFont, sizeof(wndData->hFont));
				for (int i = 0; i < tmp; i++)
					wndData->objects[i]->WriteFile(fo);

				SetWindowLongPtr(hWndChild, 0, (LONG_PTR)wndData);

				SetWindowText(hWndChild, ChildTitle);
				MessageBox(hWnd, wndData->file, L"Lưu vào file", MB_OK);
				fo.close();
			}
			else
			{
				ofstream fo;
				fo.open(wndData->file, ios::binary | ios::out);
				int tmp1, tmp = wndData->objects.size();

				WCHAR ChildTitle[256];
				wsprintf(ChildTitle, PathFindFileName(wndData->file));
				tmp1 = sizeof(WCHAR) * wcslen(wndData->file);
				fo.write((char*)&tmp1, sizeof(int));
				fo.write((char*)&wndData->file, tmp1);
				fo.write((char*)&tmp, sizeof(int));
				fo.write((char*)&wndData->color, sizeof(wndData->color));
				fo.write((char*)&wndData->hFont, sizeof(wndData->hFont));
				for (int i = 0; i < tmp; i++)
					wndData->objects[i]->WriteFile(fo);

				SetWindowLongPtr(hWndChild, 0, (LONG_PTR)wndData);

				SetWindowText(hWndChild, ChildTitle);
				MessageBox(hWnd, wndData->file, L"Lưu vào file", MB_OK);
				fo.close();
			}
		}
		else
			MessageBox(hWnd, L"Bạn chưa chọn cửa sổ để lưu", L"Lỗi: Chưa chọn cửa sổ", MB_OK);
		break;
	}
	case ID_DRAW_LINE:
	{
		OutputDebugString(L"Line mode .....\n");
		UpdateCheckMenu(hMenuDraw, wmId, SelectedDrawMode);
		break;
	}
	case ID_DRAW_RECTANGLE:
	{
		OutputDebugString(L"Rectangle mode .....\n");
		UpdateCheckMenu(hMenuDraw, wmId, SelectedDrawMode);
		break;
	}
	case ID_DRAW_ELLIPSE:
	{
		OutputDebugString(L"Ellipse mode .....\n");
		UpdateCheckMenu(hMenuDraw, wmId, SelectedDrawMode);
		break;
	}
	case ID_DRAW_TEXT:
	{
		OutputDebugString(L"Text mode .....\n");
		UpdateCheckMenu(hMenuDraw, wmId, SelectedDrawMode);
		break;
	}
	case ID_DRAW_SELECTOBJECT:
	{
		OutputDebugString(L"Select object mode .....\n");
		UpdateCheckMenu(hMenuDraw, wmId, SelectedDrawMode);
		break;
	}
	case ID_FILE_NEW:
	{
		EnableMenuItem(hMenuDraw, ID_DRAW_FONT, MF_ENABLED | MF_BYCOMMAND);
		EnableMenuItem(hMenuDraw, ID_DRAW_COLOR, MF_ENABLED | MF_BYCOMMAND);
		FileNewMenuHandle(notify, counter);
		DrawMenuBar(hWnd);
		break;
	}
	case ID_WINDOW_CASCADE: {
		SendMessage(hWndMDIClient, WM_MDICASCADE, 0L, 0L);
		break; }
	case ID_WINDOW_TIDE: {
		SendMessage(hWndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0L);
		break;
	}
	case ID_WINDOW_CLOSEALL: {
		counter = 0;
		EnableMenuItem(hMenuDraw, ID_DRAW_FONT, MF_GRAYED | MF_BYCOMMAND);
		EnableMenuItem(hMenuDraw, ID_DRAW_COLOR, MF_GRAYED | MF_BYCOMMAND);
		EnumChildWindows(hWndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
		DrawMenuBar(hWnd);
		break;
	}
	}
}