﻿ATOM                MyRegisterChildClass(HINSTANCE hInstance);

LRESULT CALLBACK    ChildWndProc(HWND, UINT, WPARAM, LPARAM);

OPENFILENAME SaveFileDialog(HWND hwnd);

OPENFILENAME OpenFileDialog(HWND hwnd);

LRESULT CALLBACK MDICloseProc(HWND hWnd, LPARAM lParam);

void InitialMDIClientWindow(HMENU hMenuDraw, HMENU hMenuEdit, HMENU hMenuWindow, HWND hWnd);

CHOOSECOLOR COLORCtrl();

CHOOSEFONT FONTCtrl();

void InitialChildWnd(HWND hWnd, int type);

void addUserToolbar(HWND hWnd);

void InitializeToolbar(HWND hWnd);

void UpdateCheckMenu(HMENU hMenuDraw, int wmId, int &SelectedDrawMode);

HWND FileNewMenuHandle(WCHAR notify[], int &counter);

HWND InputText(HWND hWnd, int left, int top);

void _Draw_OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2);
void _Edit_OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, OBJECT*obj, int type);

void _Draw_OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2);

void _Edit_OnLButtonDown(HWND hWnd, LPARAM lParam, int &type);

void _Draw_OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2, bool &inputted, HWND &hText
	, int &x_saved, int &y_saved);

 void _Edit_OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam,
	int &x1, int &x2, int &y1, int &y2, bool &inputted, HWND &hText
	, int &x_saved, int &y_saved);

void OnWmPaint(HWND hWnd);

void doEditCopyMenu(HWND hWnd);
void doEditPasteMenu(HWND hWnd);
void doEditCutMenu(HWND hWnd);
void doEditDeleteMenu(HWND hWnd);