//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by P1.rc
//
#define IDC_MYICON                      2
#define IDD_P1_DIALOG                   102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_EXIT                        105
#define IDI_P1                          107
#define IDI_SMALL                       108
#define IDC_P1                          109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     132
#define ID_FILE_OPEN                    32771
#define ID_FILE_NEW                     32772
#define ID_FILE_SAVE                    32773
#define ID_DRAW_COLOR                   32774
#define ID_DRAW_FONT                    32775
#define ID_DRAW_LINE                    32776
#define ID_DRAW_RECTANGLE               32777
#define ID_DRAW_ELLIPSE                 32778
#define ID_DRAW_TEXT                    32779
#define ID_DRAW_SELECTOBJECT            32780
#define ID_WINDOW_TIDE                  32781
#define ID_WINDOW_CASCADE               32782
#define ID_WINDOW_CLOSEALL              32783
#define ID_MODE_DRAW                    32788
#define ID_MODE_EDIT                    32789
#define ID_EDIT_CUT                     32790
#define ID_EDIT_COPY                    32791
#define ID_EDIT_PASTE                   32792
#define ID_EDIT_DELETE                  32796
#define ID_EDIT_                        32800
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32802
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
